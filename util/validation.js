function kiemTraRong(value, selectorError, name) {
  if (value === "") {
    document.getElementById(selectorError).style.display = block;
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}
