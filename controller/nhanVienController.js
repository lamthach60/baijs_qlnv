function timKiemViTri(dsnv, maNV) {
  return dsnv.findIndex(function (item) {
    return item.ma == maNV;
  });
}
var layThongTinTuForm = function () {
  var tk = document.getElementById("tknv").value;
  var hoten = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var mk = document.getElementById("password").value;
  var ngaylam = document.getElementById("datepicker").value;
  var luong = document.getElementById("luongCB").value;
  var chucvu = document.getElementById("chucvu").value;
  var giolam = document.getElementById("gioLam").value;

  var nv = new NhanVien(tk, hoten, email, mk, ngaylam, luong, chucvu, giolam);
  return nv;
};

var hienThiThongTinLenForm = function (nv) {
  document.getElementById("tknv").value = nv.tk;
  document.getElementById("name").value = nv.hoten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.mk;
  document.getElementById("datepicker").value = nv.ngaylam;
  document.getElementById("luongCB").value = nv.luong;
  document.getElementById("chucvu").value = nv.chucvu;
  document.getElementById("gioLam").value = nv.giolam;
};
