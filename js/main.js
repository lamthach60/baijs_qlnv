var dsnv = [];
// up lên local storage
var dataJson = localStorage.getItem("dsnv");
if (dataJson !== null) {
  var arrayNV = JSON.parse(dataJson);
  for (var index = 0; index < arrayNV.length; index++) {
    var item = arrayNV[index];
    var nv = new NhanVien(
      item.tk,
      item.hoten,
      item.email,
      item.mk,
      item.ngaylam,
      item.luong,
      item.chucvu,
      item.giolam
    );
    dsnv.push(nv);
  }
  renderDSNV(dsnv);
}

var luuLocalStorage = function (dsnv) {
  var dsnvJSON = JSON.stringify(dsnv);
  localStorage.setItem("dsnv", dsnvJSON);
};

//

function renderDSNV(arrayNhanVien) {
  var contentHTML = "";
  for (var index = 0; index < arrayNhanVien.length; index++) {
    var nv = arrayNhanVien[index];
    var contentTR = `<tr>
                        <td>${nv.tk}</td>
                        <td>${nv.hoten}</td>
                        <td>${nv.email}</td>
                        <td>${nv.ngaylam}</td>
                        <td>${nv.chucvu}</td>
                        <td>${nv.tinhTongluong()}</td>
                        <td>${nv.tinhXepHang()}</td>
                        <td><button class="btn btn-danger" onclick="xoaNhanVien(${
                          nv.tk
                        })">Xóa</button></td>
                        <td><button class="btn btn-danger" onclick="layThongTinNhanVien(${
                          nv.tk
                        })">Sửa</button></td>
                    </tr>`;
    contentHTML = contentHTML + contentTR;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function kiemTraRong(value, id, name) {
  if (value === "" && value == 0) {
    document.getElementById(id).style.display = "block";
    document.getElementById(id).innerHTML = "Nhập " + name;
    return false;
  }
  document.getElementById(id).style.display = "none";
  return true;
}
function kiemTraEmail(value, id, name) {
  var regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  if (regexEmail.test(value)) {
    document.getElementById(id).style.display = "none";
    return true;
  }
  document.getElementById(id).style.display = "block";
  document.getElementById(id).innerHTML = "Sai định dạng  " + name;
  return false;
}
function kiemTraTatCaKyTu(value, id, name) {
  var regexLetter = /^[A-Z a-z]+$/; //Nhập các kí tự a->z A->Z hoặc khoảng trống không bao gồm unicode
  if (regexLetter.test(value)) {
    //test nếu ok false
    document.getElementById(id).style.display = "none";
    return true;
  }
  document.getElementById(id).style.display = "block";
  document.getElementById(id).innerHTML = "Sai định dạng  " + name;
  return false;
}
function kiemTraPassWord(value, id, name) {
  var regexPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/; //Nhập các kí tự a->z A->Z hoặc khoảng trống không bao gồm unicode
  if (regexPass.test(value)) {
    //test nếu ok false
    document.getElementById(id).style.display = "none";
    return true;
  }
  document.getElementById(id).style.display = "block";
  document.getElementById(id).innerHTML = "Sai định dạng  " + name;
  return false;
}
function kiemTraGioLam(value, id, name) {
  if (value < 80 && value > 200) {
    document.getElementById(id).style.display = "block";
    document.getElementById(id).innerHTML = "Sai định dạng  " + name;
    return false;
  } else {
    document.getElementById(id).style.display = "none";
    return true;
  }
}
function timKiemViTri(dsnv, MaNV) {
  return dsnv.findIndex(function (item) {
    return item.tk == MaNV;
  });
}

function themNguoiDung() {
  var nv = layThongTinTuForm();
  dsnv.push(nv);
  var valid = true;
  valid &= kiemTraRong(nv.tk, "tbTKNV", "Tài khoản");
  valid &= kiemTraRong(nv.hoten, "tbTen", "Tên");
  valid &= kiemTraRong(nv.email, "tbEmail", "Email");
  valid &= kiemTraRong(nv.ngaylam, "tbNgay", "Ngày làm");
  valid &= kiemTraRong(nv.mk, "tbMatKhau", "Mật khẩu");

  valid &= kiemTraRong(nv.chucvu, "tbChucVu", "Chức vụ");
  if (nv.chucvu == "") {
    console.log("cc");
    let ngaySai = "Hãy nhập lương";
    document.getElementById("tbChucVu").style.display = "block";
    document.getElementById("tbChucVu").innerHTML = ngaySai;
  }
  valid &= kiemTraRong(nv.giolam, "tbGiolam", "Giờ làm");
  valid &= kiemTraEmail(nv.email, "tbEmail_error", "Email");
  valid &= kiemTraTatCaKyTu(nv.hoten, "tbTenError", "Họ và tên");
  valid &= kiemTraPassWord(nv.mk, "tbMatKhauError", "Mật khẩu");
  valid &= kiemTraGioLam(nv.giolam, "tbGiolamError", "Giờ làm");
  if (nv.luong == "") {
    console.log("cc");
    var luongSai = "Hãy nhập lương";
    document.getElementById("tbLuongCB").style.display = "block";
    document.getElementById("tbLuongCB").innerHTML = luongSai;
    return false;
  }
  if (!valid) {
    return;
  }
  var isValid = kiemTraMaNhanVien(dsnv, nv.tk);
  if (isValid) {
    dsnv.push(nv);
    renderDSNV(dsnv);
    luuLocalStorage(dsnv);
  }
}
function getStore(storeName) {
  var output;
  if (localStorage.getItem(storeName)) {
    output = JSON.parse(localStorage.getItem(storeName));
  }
  return output;
}

function xoaNhanVien(maNV) {
  console.log("yes");
  var viTri = timKiemViTri(dsnv, maNV);
  console.log(viTri);
  dsnv.splice(viTri, 1);
  renderDSNV(dsnv);
  luuLocalStorage(dsnv);
}
function layThongTinNhanVien(maNV) {
  document.getElementById("myModal").style.display = "block";
  document.getElementById("myModal").style.overflow = "visible";
  document.getElementById("myModal").style.opacity = "1";
  document.getElementById("myModal").style.top = "170px";
  console.log("maNV: ", maNV);
  var viTri = timKiemViTri(dsnv, nv.tk);
  console.log("viTri: ", viTri);
  if (viTri == -1) {
    return;
  }
  var nhanVien = dsnv[viTri];
  document.getElementById("tknv").disabled = true;
  hienThiThongTinLenForm(nhanVien);
}

function capNhatNhanVien() {
  document.getElementById("tknv").disabled = false;
  var nv = layThongTinTuForm();
  var viTri = timKiemViTri(dsnv, nv.tk);
  dsnv[viTri] = nv;
  renderDSNV(dsnv);
  luuLocalStorage(dsnv);
}
window.onload = function () {
  var content = getStore("dsnv");
  if (content) {
    renderDSNV(content);
  }
};
