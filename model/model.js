var NhanVien = function (
  _tk,
  _hoten,
  _email,
  _mk,
  _ngaylam,
  _luong,
  _chucvu,
  _giolam
) {
  this.tk = _tk;
  this.hoten = _hoten;
  this.email = _email;
  this.mk = _mk;
  this.ngaylam = _ngaylam;
  this.luong = _luong;
  this.chucvu = _chucvu;
  this.giolam = _giolam;
  this.tienLuongSep = _luong * 3;
  this.tienLuongTp = _luong * 2;
  this.tinhTongluong = function () {
    if (this.chucvu == "Sếp") {
      return this.tienLuongSep;
    } else if (this.chucvu == "Trưởng phòng") {
      return this.tienLuongTp;
    } else {
      return this.luong;
    }
  };
  this.tinhXepHang = function () {
    if (this.giolam >= 192) {
      return "Nhân viên xuất sắc";
    }
    if (this.giolam >= 176) {
      return "Nhân viên giỏi";
    }
    if (this.giolam >= 160) {
      return "Nhân viên khá";
    }
    if (this.giolam < 160) {
      return "Nhân viên trung bình";
    }
  };
};
